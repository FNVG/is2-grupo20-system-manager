import pytest as pytest

from django.test import Client
from django.test import TestCase




@pytest.mark.urls('oauth_project.urls')
def test_localhost(client):
  response = client.get('http://127.0.0.1:8000/')
  assert response.status_code == 200


def test_admin_url(client):
  response = client.get('http://127.0.0.1:8000/admin/')
  assert response['location'] == '/admin/login/?next=/admin/'

def test_logout_url(client):
  response = client.get('http://127.0.0.1:8000/logout')
  assert response['location'] == '/'




