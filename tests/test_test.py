from unittest import TestCase

def func(x):
    return x + 1


class Test(TestCase):
    def test_test_answer(self):
        assert func(3) == 4

    def test_test_answer(self):
        assert func(2) == 3
